package com.example.mealsandrecipes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class FoodDetails extends AppCompatActivity {

    // now we will get data from intent and set to UI

    ImageView imageView;
    TextView itemName, itemPrice, itemRating;
    RatingBar ratingBar;
    ElegantNumberButton numberButton;
    FloatingActionButton addCartBtn;

    String name, price, rating, imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_details);


        Intent intent = getIntent();


        name = intent.getStringExtra("name");
        price = intent.getStringExtra("price");
        rating = intent.getStringExtra("rating");
        imageUrl = intent.getStringExtra("image");

        imageView = findViewById(R.id.imageView5);
        itemName = findViewById(R.id.name);
        itemPrice = findViewById(R.id.price);
        itemRating = findViewById(R.id.rating);
        ratingBar = findViewById(R.id.ratingBar);

        Glide.with(getApplicationContext()).load(imageUrl).into(imageView);
        itemName.setText(name);
        itemPrice.setText("$ "+price);
        itemRating.setText(rating);
        ratingBar.setRating(Float.parseFloat(rating));

    }

    public void backImage(View view) {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
        Log.i("backImage", "finish " );

       finish();
    }


    public void addTocart( View view) {
        Intent intent = new Intent(this,CartActivity.class);
        startActivity(intent);
        Log.i("backImage", "finish " );
    }

    public void shoppingCartItem(View view) {
        Intent intent = new Intent(this,CartActivity.class);
        startActivity(intent);
    }
}