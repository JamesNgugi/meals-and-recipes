package com.example.mealsandrecipes;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mealsandrecipes.Api.ApiInterface;
import com.example.mealsandrecipes.Api.RetrofitClient;
import com.example.mealsandrecipes.adapter.AllMenuAdapter;
import com.example.mealsandrecipes.adapter.PopularAdapter;
import com.example.mealsandrecipes.adapter.RecommendedAdapter;
import com.example.mealsandrecipes.model.Allmenu;
import com.example.mealsandrecipes.model.FoodData;
import com.example.mealsandrecipes.model.Popular;
import com.example.mealsandrecipes.model.Recommended;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



    public class MainActivity extends AppCompatActivity {
        ApiInterface apiInterface;
        RecyclerView popularRecyclerView, recommendedRecyclerView, allMenuRecyclerView;
        EditText searchView;
        CharSequence search ="";
        PopularAdapter popularAdapter;
        RecommendedAdapter recommendedAdapter;
        AllMenuAdapter allMenuAdapter;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            searchView = findViewById(R.id.search_bar);

            apiInterface = RetrofitClient.getRetrofitInstance().create(ApiInterface.class);

            Call<List<FoodData>> call = apiInterface.getAllData();
            call.enqueue(new Callback<List<FoodData>>() {
                @Override
                public void onResponse(Call<List<FoodData>> call, Response<List<FoodData>> response) {

                    List<FoodData> foodDataList = response.body();


                    getPopularData(foodDataList.get(0).getPopular());

                    getRecommendedData(foodDataList.get(0).getRecommended());

                    getAllMenu(foodDataList.get(0).getAllmenu());

                    searchView.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                          allMenuAdapter.getFilter().filter(charSequence);
                          search =charSequence;
                        }

                        @Override
                        public void afterTextChanged(Editable editable) {

                        }
                    });

                }

                @Override
                public void onFailure(Call<List<FoodData>> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "Server is not responding.", Toast.LENGTH_SHORT).show();
                }
            });


        }

        private void getPopularData(List<Popular> popularList) {

            popularRecyclerView = findViewById(R.id.popular_recycler);
            popularAdapter = new PopularAdapter(this, popularList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            popularRecyclerView.setLayoutManager(layoutManager);
            popularRecyclerView.setAdapter(popularAdapter);

        }

        private void getRecommendedData(List<Recommended> recommendedList) {

            recommendedRecyclerView = findViewById(R.id.recommended_recycler);
            recommendedAdapter = new RecommendedAdapter(this, recommendedList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recommendedRecyclerView.setLayoutManager(layoutManager);
            recommendedRecyclerView.setAdapter(recommendedAdapter);

        }

        private void getAllMenu(List<Allmenu> allmenuList) {

            allMenuRecyclerView = findViewById(R.id.all_menu_recycler);
            allMenuAdapter = new AllMenuAdapter(this, allmenuList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            allMenuRecyclerView.setLayoutManager(layoutManager);
            allMenuRecyclerView.setAdapter(allMenuAdapter);
            allMenuAdapter.notifyDataSetChanged();

        }

        public void shoppingCartItem(View view) {

            Intent intent = new Intent(this,CartActivity.class);

            startActivity(intent);
            Log.i("backImage", "finish " );

           // finish();

        }
    }


